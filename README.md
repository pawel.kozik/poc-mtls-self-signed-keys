# PoC of mTLS connection

Purpose: demonstrate example mTLS setup with root, intermediate and leaf certificates.

## Instructions

### Re-generate keys & certificates (optional)

```shell
make gen
```

### Build docker container

```shell
make build
```

### Run docker container

```shell
make up
```

### Simulate client

```shell
make test
```
