const https = require("https");
const fs = require("fs");

const options = {
  hostname: "localhost",
  port: 443,
  path: "/",
  method: "GET",
  key: fs.readFileSync("storage/client.key"),
  cert: fs.readFileSync("storage/bundle.crt"),
  ca: fs.readFileSync("storage/server.crt"), // Normally trusted store is checked but in this example server certificate is self-signed
};

const req = https.request(options, (res) => {
  console.log("Status: ", res.statusCode);

  res.on("data", (data) => {
    process.stdout.write(data);
  });
});

req.on("error", (e) => {
  console.error(e);
});
req.end();
