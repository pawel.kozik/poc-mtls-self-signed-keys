build:
	cd server && docker build -t nginx-mtls .

up:
	cd server && docker run -d -p 443:443 nginx-mtls

down:
	docker ps -q --filter ancestor=nginx-mtls | xargs docker stop

test:
	cd client && node client.js

ssh:
	@CONTAINER_ID=$$(docker ps -q --filter ancestor=nginx-mtls); \
	docker exec -it $$CONTAINER_ID /bin/bash

logs:
	@CONTAINER_ID=$$(docker ps -q --filter ancestor=nginx-mtls); \
	docker logs $$CONTAINER_ID

gen-root:
	openssl genrsa -out gen/root.key 2048
	openssl x509 -new -key gen/root.key -sha256 -extfile ca.conf -extensions v3_req -subj '/C=LT/O=kevin/CN=kevin' -days 3650 -out gen/root.crt

gen-intermediate:
	openssl genrsa -out gen/intermediate.key 2048
	openssl req -new -key gen/intermediate.key -out gen/intermediate.csr -subj '/C=LT/O=kevin/CN=kevin'
	openssl x509 -req -in gen/intermediate.csr -CA gen/root.crt -CAkey gen/root.key -CAcreateserial -out gen/intermediate.crt -days 500 -sha256 -extfile ca.conf -extensions v3_req

gen-leaf:
	openssl genrsa -out gen/client.key 2048
	openssl req -new -key gen/client.key -out gen/client.csr -subj '/C=PL/O=partner/CN=partner'
	openssl x509 \
		-req \
		-in gen/client.csr \
		-CA gen/intermediate.crt \
		-CAkey gen/intermediate.key \
		-CAcreateserial \
		-out gen/leaf.crt \
		-days 500 \
		-sha256 \
		-subj '/C=PL/O=partner/CN=partnerId:aaaaa'
	cat gen/leaf.crt gen/intermediate.crt > gen/bundle.crt

gen-server:
	openssl genrsa -out server/storage/server.key 2048
	openssl req -new -x509 -key server/storage/server.key -sha256 -subj '/C=LT/O=kevin/CN=localhost' -days 3650 -out server/storage/server.crt

gen-move:
	cp gen/root.crt server/storage/
	cp gen/intermediate.crt client/storage/
	cp gen/client.key client/storage/
	cp gen/leaf.crt client/storage/
	cp gen/bundle.crt client/storage/
	cp gen/server.crt client/storage/
	cp gen/server.key server/storage/

# Usage: make crt-print crt=gen/leaf.crt
crt-print:
	openssl x509 -in $(crt) -text
